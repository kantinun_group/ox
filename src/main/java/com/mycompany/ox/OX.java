/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.ox;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class OX {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int x, y;
        char board[][] = new char[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }

        System.out.println("Welcome to OX Game");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println("Turn O");
        System.out.println("Please input row, col:");
        x = kb.nextInt();
        y = kb.nextInt();
        board[x - 1][y - 1] = 'O';
        char ch = 'O';
        int count = 0;
        while (true) {
            System.out.println();
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    System.out.print(board[i][j] + " ");
                }
                System.out.println();
            }
            count++;
            //check horizontal
            if (board[0][0] != '-' && board[0][0] == board[0][1] && board[0][0] == board[0][2]) {
                System.out.println(">>>" + ch + " Win<<<");
                break;
            } else if (board[1][0] != '-' && board[1][0] == board[1][1] && board[1][0] == board[1][2]) {
                System.out.println(">>>" + ch + " Win<<<");
                break;
            } else if (board[2][0] != '-' && board[2][0] == board[2][1] && board[2][0] == board[2][2]) {
                System.out.println(">>>" + ch + " Win<<<");
                break;
            } //check vertical
            else if (board[0][0] != '-' && board[0][0] == board[1][0] && board[0][0] == board[2][0]) {
                System.out.println(">>>" + ch + " Win<<<");
                break;
            } else if (board[0][1] != '-' && board[0][1] == board[1][1] && board[0][1] == board[2][1]) {
                System.out.println(">>>" + ch + " Win<<<");
                break;
            } else if (board[0][2] != '-' && board[0][2] == board[1][2] && board[0][2] == board[2][2]) {
                System.out.println(">>>" + ch + " Win<<<");
                break;
            } //check diagonal
            else if (board[0][0] != '-' && board[0][0] == board[1][1] && board[0][0] == board[2][2]) {
                System.out.println(">>>" + ch + " Win<<<");
                break;
            } else if (board[2][0] != '-' && board[2][0] == board[1][1] && board[2][0] == board[0][2]) {
                System.out.println(">>>" + ch + " Win<<<");
                break;
            } else {
                if (count == 9) {
                    System.out.println(">>>Draw<<<");
                    break;
                }
                if (count % 2 == 0) {
                    ch = 'O';
                } else {
                    ch = 'X';
                }
                System.out.println("Turn " + ch);
                x = kb.nextInt();
                y = kb.nextInt();
                board[x - 1][y - 1] = ch;
            }

        }
    }
}
